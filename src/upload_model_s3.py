#!/usr/bin/env python
#-*- coding: utf-8 -*-
import boto3
import click
from dotenv import load_dotenv

load_dotenv('.env')


@click.command()
@click.option('--model_path', default='models/model_1.joblib')
@click.option('--bucket_name', default='pabdcv-3m')
@click.option('--s3_model_name', default='model_123456.joblib')
def upload_model_s3(model_path, bucket_name, s3_model_name):
    session = boto3.session.Session()
    s3 = session.client(
        service_name='s3',
        endpoint_url='https://storage.yandexcloud.net',
    )

    s3.upload_file(model_path, bucket_name, s3_model_name)

    # Получить список объектов в бакете
    for key in s3.list_objects(Bucket=bucket_name)['Contents']:
        print(key['Key'])


if __name__ == '__main__':
    upload_model_s3()
