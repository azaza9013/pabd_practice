from requests import get  # to make GET request


def download(url, file_name):
    # open in binary mode
    with open(file_name, "wb") as file:
        # get request
        response = get(url)
        # write to file
        file.write(response.content)


if __name__ == '__main__':
    url = 'https://drive.google.com/file/d/1E76GQPM750em2xAEPHDsK4d9m7MUulQS/view?usp=sharing'
    file_name = 'data/raw/cian/' + url.split('/')[-1]
    download(url, file_name)
